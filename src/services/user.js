import axios from 'axios'
import queryString from 'query-string'

const list = async (data, filters) => {
  try {
    const query = await queryString.stringify(data)
    const res = await axios.get(`/users?${query}&${filters}`)
    return res.data
  } catch (err) {
    throw err
  }
}

export default {
  list
}
