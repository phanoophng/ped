import React, { Component } from 'react';
import {
  DropDownMenu,
  MenuItem,
  TextField
} from 'material-ui'

const styles = {
  radio: {
    marginRight: 0,
    marginLeft: 10
  }
}

class Filter extends Component {
  state = {
    filterType: 'gender',
    gender: '',
    ageType: '',
    from: '',
    to: '',
    age: ''
  }

  onGenderChange = (e, key, value) => {
    this.setState({
      gender: value
    })
  }

  onAgeTypeChange = (e, key, value) => {
    this.setState({
      ageType: value
    })
  }

  onChooseFilterType = (value) => {
    this.setState({
      filterType: value,
      gender: '',
      ageType: ''
    })
  }

  handleAgeFilter = (id) => {
    const { onSubmitFilter } = this.props
    const { age, ageType, from, to } = this.state
    let data = null
    if (ageType === 'specific') {
      if (age === null || age === '') {
        alert('Please enter age.')
        return
      } else {
        data = { id, type: 'age', value: age }
      }
    } else if (ageType === 'range') {
      if (from.length > 0 && to.length > 0) {
        if ( parseInt(to) > parseInt(from)) {
          data = { id, type: 'ageRange', value: { from, to } }
        } else {
          alert('"To" should greater than "From".')
          return
        }
      } else {
        alert('Please enter age.')
        return
      }
    } else {
      alert('Please choose age type for filter.')
      return
    }

    onSubmitFilter(data)
  }

  onSubmit = async () => {
    const { filterType, gender } = this.state
    const { onSubmitFilter } = this.props
    const date = new Date()
    const id = await date.getTime()
    switch (filterType) {
      case 'gender':
        const data = { id, type: 'gender', value: gender }
        onSubmitFilter(data)
        break
      case 'age':
        this.handleAgeFilter(id)
        break
      default:
        alert('Please choose filter type.')
        break
    }
  }

  renderAgeInput = type => {
    const { from, to, age } = this.state
    if (type === 'specific') {
      return <TextField value={age} onChange={(e, value) => this.setState({ age: value }) } name="age-input" className="age-input" placeholder="Age" />
    } else if (type === 'range') {
      return (
        <div className="row">
          <TextField value={from} onChange={(e, value) => this.setState({ from: value }) } name="from" className="age-input" placeholder="from" />
          <TextField value={to} onChange={(e, value) => this.setState({ to: value }) } name="to" className="age-input" placeholder="to" />
        </div>
      )
    }
  }

  render () {
    const { filterType, gender, ageType } = this.state
    return (
      <div className="filters-wrapper">
        <div className="filters">
          <input type="radio" checked={filterType === 'gender'} onChange={() => this.onChooseFilterType('gender')} style={styles.radio} />
          <DropDownMenu
            value={gender}
            onChange={this.onGenderChange}
            placeholder="Gender"
            className="dropdown-button"
            disabled={filterType !== 'gender'}
          >
            <MenuItem value="" primaryText="GENDER: ALL" />
            <MenuItem value="male" primaryText="GENDER: MALE" />
            <MenuItem value="female" primaryText="GENDER: FEMALE" />
          </DropDownMenu>
          <input type="radio" checked={filterType === 'age'} onChange={() => this.onChooseFilterType('age')} style={styles.radio} />
          <DropDownMenu
            value={ageType}
            placeholder="Age"
            onChange={this.onAgeTypeChange}
            className="dropdown-button"
            disabled={filterType !== 'age'}
          >
            <MenuItem value="" primaryText="AGE: ALL" />
            <MenuItem value="specific" primaryText="AGE: SPECIFIC" />
            <MenuItem value="range" primaryText="AGE: RANGE" />
          </DropDownMenu>
          {this.renderAgeInput(ageType)}
          <button className="button is-primary" style={{ marginRight: 10 }} onClick={this.onSubmit}>
            + ADD
          </button>
        </div>
      </div>
    )
  }
}

export default Filter
