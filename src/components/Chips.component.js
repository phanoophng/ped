import React from 'react'
import { Chip } from 'material-ui'

const styles = {
  chip: {
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 10
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap'
  }
}

const renderChip = (data, index, onDelete) => {
  const { type, value } = data
  switch (type) {
    case 'age':
      return (
        <Chip key={index} style={styles.chip} onRequestDelete={() => onDelete(data)}>
          {`Age: ${value}`}
        </Chip>
      )
    case 'ageRange':
      return (
        <Chip key={index} style={styles.chip} onRequestDelete={() => onDelete(data)}>
          {`Age: ${value.from} - ${value.to}`}
        </Chip>
      )
    case 'gender':
      return (
        <Chip key={index} style={styles.chip} onRequestDelete={() => onDelete(data)}>
          {`Gender: ${value}`}
        </Chip>
      )
    case 'search':
      if (value.length > 0) {
        return (
          <Chip key={index} style={styles.chip} onRequestDelete={() => onDelete(data)}>
            {`Search: ${value}`}
          </Chip>
        )
      }
      break
    default:
      break
  }
}

const renderChips = (data, onDelete) => {
  return data.map((d, index) => renderChip(d, index, onDelete))
}

const Chips = ({ data, onDelete }) => {
  return <div style={styles.wrapper}>{renderChips(data, onDelete)}</div>
}

export default Chips
