import React from 'react'
import { TextField } from 'material-ui'
import PropsTypes from 'prop-types'

const Search = ({ onChange, value, submitSearch }) => {
  return (
    <div className="search">
      <div className="search-box">
        <i className="fa fa-search" />
        <TextField
          fullWidth
          placeholder="Search with first name, last name, id, email"
          onChange={onChange}
          value={value}
          onKeyPress={submitSearch}
        />
      </div>
    </div>
  )
}

Search.propTypes = {
  onSearch: PropsTypes.func,
  submitSearch: PropsTypes.func,
  value: PropsTypes.string
}

export default Search
