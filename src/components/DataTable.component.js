import React from 'react'

const UserRow = ({ data }) => {
  const {
    id,
    first_name: firstName,
    last_name: lastName,
    email,
    age,
    gender
  } = data
  return (
    <tr>
      <td>{id}</td>
      <td>{firstName}</td>
      <td>{lastName}</td>
      <td>{email}</td>
      <td>{age}</td>
      <td>{gender}</td>
    </tr>
  )
}

const renderUsers = users => {
  return users.map((user, index) => <UserRow key={index} data={user} />)
}

const DataTable = ({ users }) => {
  return (
    <div className="table-responsive">
      <table className="table main-table">
        <thead>
          <tr>
            <th>ID</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Age</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>{renderUsers(users)}</tbody>
      </table>
    </div>
  )
}

export default DataTable
