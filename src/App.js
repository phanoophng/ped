import React, { Component } from 'react'
import axios from 'axios'
import queryString from 'query-string'
import Pagination from 'material-ui-pagination'
import { DropDownMenu, MenuItem } from 'material-ui'

import UserService from './services/user'
import DataTable from './components/DataTable.component'
import Search from './components/Search.component'
import Filter from './components/Filter.component'
import Chips from './components/Chips.component'

import './App.css'
import 'bulma/css/bulma.css'

axios.defaults.baseURL = 'http://localhost:4000'

class App extends Component {
  state = {
    users: [],
    searchText: '',
    limit: 10,
    start: 0,
    gender: '',
    ageType: '',
    filters: [],
    total: 0,
    itemPerPage: 10,
    currentPage: 1,
    sortBy: 'id'
  }
  componentDidMount = () => {
    const { start, limit } = this.state
    this.setState({ isLoading: false })
    this.fetchData({ start, limit })
  }

  fetchData = (options, query) => {
    UserService.list(options, query)
      .then(data => {
        const { users, total } = data
        this.setState({ users, isLoading: false, total: parseInt(total) })
      })
      .catch(err => alert(`${err.message}`))
  }

  onSearchTextChange = e => {
    const { value } = e.target
    this.setState({
      searchText: value
    })
  }

  handleFilter = data => {
    const { type, value } = data
    switch (type) {
      case 'ageRange':
        return queryString.stringify({ minAge: value.from, maxAge: value.to })
      default:
        return queryString.stringify({ [type]: value })
    }
  }

  onFilter = async () => {
    const { filters, itemPerPage, currentPage, sortBy } = this.state
    const options = [ ...filters, { type: 'sort', value: sortBy } ]
    const data = await options.map(this.handleFilter)
    const query = await data.join('&')
    const limit = itemPerPage
    const start = itemPerPage * currentPage - itemPerPage
    this.fetchData({ start, limit }, query)
  }

  onSubmitFilter = newFilterData => {
    const { filters } = this.state
    this.setState(
      {
        filters: [...filters, newFilterData],
        currentPage: 1
      },
      () => {
        this.onFilter()
      }
    )
  }

  _deleteFilter = async data => {
    const { id, type } = data
    const { filters } = this.state
    let { searchText } = this.state
    const newFilters = await filters.filter(f => f.id !== id)
    if (type === 'search') {
      searchText = ''
    }
    this.setState(
      {
        filters: newFilters,
        searchText
      },
      () => {
        this.onFilter()
      }
    )
  }

  onSubmitSearch = async e => {
    const { searchText, filters } = this.state
    if (e.key === 'Enter' && searchText.length > 0) {
      const date = new Date()
      const id = await date.getTime()
      this.setState(
        {
          filters: [...filters, { id, type: 'search', value: searchText }],
          searchText: '',
          currentPage: 1
        },
        () => {
          this.onFilter()
        }
      )
    }
  }

  onPageChange = page => {
    this.setState(
      {
        currentPage: page
      },
      () => {
        this.onFilter()
      }
    )
  }

  renderAmountSelector = () => {
    const options = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    return options.map(amount => (
      <MenuItem value={amount} primaryText={`${amount} items / page`} />
    ))
  }

  renderSortOptions = () => {
    const options = [
      { value: 'id', label: 'id' },
      { value: 'first_name', label: 'First name' },
      { value: 'last_name', label: 'Last name' },
      { value: 'email', label: 'Email' },
      { value: 'gender', label: 'Gender' },
      { value: 'age', label: 'Age' }
    ]
    return options.map(option => (
      <MenuItem value={option.value} primaryText={`Sort by: ${option.label}`} />
    ))
  }

  onAmountChange = (e, index, value) => {
    this.setState(
      {
        itemPerPage: value,
        page: 1
      },
      () => {
        this.onFilter()
      }
    )
  }

  onSortOptionChange = (e, index, value) => {
    this.setState(
      {
        sortBy: value,
        page: 1
      }, () => {
        this.onFilter()
      })
  }

  render () {
    const {
      users,
      searchText,
      filters,
      total,
      itemPerPage,
      sortBy
    } = this.state
    const pages = Math.ceil(total / itemPerPage)
    return (
      <div className="main">
        <div className="container">
          <div className="content has-text-centered">
            <div className="columns">
              <div className="column">
                <Search
                  submitSearch={this.onSubmitSearch}
                  onChange={this.onSearchTextChange}
                  value={searchText}
                />
              </div>
              <div className="column">
                <Filter onSubmitFilter={this.onSubmitFilter} />
              </div>
            </div>
            <Chips data={filters} onDelete={this._deleteFilter} />
            <br />
            <div className="table-control">
              <DropDownMenu
                value={sortBy}
                placeholder="Sort by"
                onChange={this.onSortOptionChange}
                className="amount-selector"
              >
                {this.renderSortOptions()}
              </DropDownMenu>
              <DropDownMenu
                value={itemPerPage}
                placeholder=""
                onChange={this.onAmountChange}
                className="amount-selector"
              >
                {this.renderAmountSelector()}
              </DropDownMenu>
            </div>
            <DataTable users={users} />
            <Pagination
              total={pages}
              current={this.state.currentPage}
              display={10}
              onChange={this.onPageChange}
              styleRoot={{ backgroundColor: '#FFF', padding: 10 }}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default App
